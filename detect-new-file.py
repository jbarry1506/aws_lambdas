import json
import boto3
import time
import datetime


def months_to_numbers(month):
    try:
        if (month == "Jan" or month == "January" or month == "1" or month == 1):
            mon = '01'
        if (month == "Feb" or month == "February" or month == "2" or month == 2):
            mon = '02'
        if (month == "Mar" or month == "March" or month == "3" or month == 3):
            mon = '03'
        if (month == "Apr" or month == "April" or month == "4" or month == 4):
            mon = '04'
        if (month == "May" or month == "5" or month == 5):
            mon = '05'
        if (month == "Jun" or month == "June" or month == "6" or month == 6):
            mon = '06'
        if (month == "Jul" or month == "July" or month == "7" or month == 7):
            mon = '07'
        if (month == "Aug" or month == "August" or month == "8" or month == 8):
            mon = '08'
        if (month == "Sep" or month == "September" or month == "9" or month == 9):
            mon = '09'
        if (month == "Oct" or month == "October" or month == 10):
            mon = '10'
        if (month == "Nov" or month == "November" or month == 11):
            mon = '11'
        if (month == "Dec" or month == "December" or month == 12):
            mon = '12'
    except exception as e:
        print(e)
        print("This is not a recognizable month.")
        print(month)
    return mon
    

def lambda_handler(event, context):
    # set up codepipeline client
    pl_client = boto3.client('codepipeline')
    # set up cloudwatch client
    cw_client = boto3.client('cloudwatch')
    # set up codecommit
    cc_client = boto3.client('codecommit')

    # check to see if there is a new file in codecommit
    cc_response = cc_client.get_folder(
        repositoryName='jcbhub_dev',
        folderPath='dummy_folder'
        )

    cc_commit_id = cc_response['commitId']
    cc_folder_path = cc_response['folderPath']
    cc_treeid = cc_response['treeId']
    cc_subfolders = cc_response['subFolders']
    cc_absolutepath = cc_response['files'][0]['absolutePath']
    cc_date = cc_response['ResponseMetadata']['HTTPHeaders']['date']
    
    # make sure AWS is happy with the datetime
    cc_date_split = cc_date.split(" ")
    cc_month = months_to_numbers(cc_date_split[2])
    cc_first_half = cc_date_split[3]+"-"+cc_month+"-"+cc_date_split[1]
    cc_second_split = cc_date_split[4].split(":")
    cc_second_half = cc_second_split[0]+":"+cc_second_split[1]+":00.000Z"
    cc_acceptable_time = cc_first_half+"T"+cc_second_half
    # print(cc_acceptable_time)    
    # print("2022-02-10T05:30:00.000Z")
    
    cc_response_commit = cc_client.get_commit(
        repositoryName = 'jcbhub_dev',
        commitId= cc_commit_id
        )
        
    cc_message = cc_response_commit['commit']['message']

    
    # CloudWatch API DashboardBody Setup
    cw_json = { 
        "widgets": [ 
            { 
                "height": 6,
                "width": 6,
                "y": 0,
                "x": 0,
                "type": "metric",
                "properties": {
                    "metrics": [
                        [ "AWS/Events", "TriggeredRules", "RuleName", "jcbhub-update-cw-dashboard", { "stat": "Sum", "id": "m0" } ]
                    ],
                    "legend": {
                        "position": "bottom"
                    },
                    "period": 300,
                    "view": "timeSeries",
                    #"stacked": false,
                    "title": "All resources - TriggeredRules",
                    "region": "us-east-1",
                    "annotations": {
                        "vertical": [
                            {
                                "label": cc_message,
                                "value": cc_acceptable_time,
                                "fill": "after"
                            }
                        ]
                    }
                }
            }
        ]
    }
    print(cc_folder_path)
    if ( "dummy_folder" in cc_folder_path ):
        # // Post to API - COMMENT FORMATTED TO TEST INSTRUCTION SPECIFICATION 

        cw_response = cw_client.put_dashboard(
                DashboardName = "jcbhub",
                DashboardBody = json.dumps(cw_json,indent=4)
            )
        print(cw_response)


    return {
        'statusCode': 200,
        'body': json.dumps(cc_response)
    }

